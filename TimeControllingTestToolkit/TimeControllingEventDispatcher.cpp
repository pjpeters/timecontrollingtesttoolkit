#include "TimeControllingEventDispatcher.h"
#include "TimeControllingEventDispatcher_p.h"

#include "NotImplemented.h"

#include <QCoreApplication>
#include <QDebug>

TimeControllingEventDispatcher::TimeControllingEventDispatcher()
    : d_ptr(new TimeControllingEventDispatcherPrivate)
{
}

TimeControllingEventDispatcher::~TimeControllingEventDispatcher()
{
}

bool TimeControllingEventDispatcher::processEvents(QEventLoop::ProcessEventsFlags)
{
    Q_D(TimeControllingEventDispatcher);

    foreach (int timerId, d->activatedTimers) {
        QSharedPointer<TimerData> timer = d->timers[timerId];
        QTimerEvent event(timerId);
        QCoreApplication::sendEvent(timer->object, &event);
    }

    d->activatedTimers.clear();

    return false;
}

bool TimeControllingEventDispatcher::hasPendingEvents()
{
    Q_D(TimeControllingEventDispatcher);

    return d->activatedTimers.size() > 0;
}

void TimeControllingEventDispatcher::registerTimer(int timerId, int interval, Qt::TimerType timerType, QObject *object)
{
    Q_D(TimeControllingEventDispatcher);

    QSharedPointer<TimerData> data(new TimerData(timerId, object, interval, timerType));
    data->nextActivation = d->now + data->interval;
    d->timers.insert(timerId, data);
}

bool TimeControllingEventDispatcher::unregisterTimer(int timerId)
{
    Q_D(TimeControllingEventDispatcher);

    if (d->timers.contains(timerId)) {
        d->timers.remove(timerId);
    }
    else {
        qWarning() << "Trying to unregister timer with ID " << timerId << "which is not registered.";
        return false;
    }
    return true;
}

bool TimeControllingEventDispatcher::unregisterTimers(QObject *object)
{
    Q_D(TimeControllingEventDispatcher);


    foreach (QSharedPointer<TimerData> const& data, d->timers) {
        if (data->object == object) {
            d->timers.remove(data->timerID);
            d->activatedTimers.removeAll(data->timerID);
        }
    }

    return true;
}

QList<QAbstractEventDispatcher::TimerInfo> TimeControllingEventDispatcher::registeredTimers(QObject *object) const
{
    const Q_D(TimeControllingEventDispatcher);

    QList<TimerInfo> theObjectsTimers;

    foreach (QSharedPointer<TimerData> const& data, d->timers) {
        if (data->object == object) {
            TimerInfo info(data->timerID, data->interval, data->timerType);
            theObjectsTimers.append(info);
        }
    }

    return theObjectsTimers;
}

int TimeControllingEventDispatcher::remainingTime(int timerId)
{
    Q_D(TimeControllingEventDispatcher);

    if (!d->timers.contains(timerId)) {
        return -1;
    }

    QSharedPointer<TimerData> const& data = d->timers[timerId];
    int timeLeft = data->nextActivation - d->now;
    if (timeLeft < 0) {
        timeLeft = 0;
    }

    return timeLeft;
}

void TimeControllingEventDispatcher::registerSocketNotifier(QSocketNotifier *)
{
    throw NotImplemented(__FUNCTION__);
}

void TimeControllingEventDispatcher::unregisterSocketNotifier(QSocketNotifier *)
{
    throw NotImplemented(__FUNCTION__);
}

bool TimeControllingEventDispatcher::registerEventNotifier(QWinEventNotifier *)
{
    throw NotImplemented(__FUNCTION__);
}

void TimeControllingEventDispatcher::unregisterEventNotifier(QWinEventNotifier *)
{
    throw NotImplemented(__FUNCTION__);
}

void TimeControllingEventDispatcher::wakeUp()
{
    throw NotImplemented(__FUNCTION__);
}

void TimeControllingEventDispatcher::interrupt()
{
    throw NotImplemented(__FUNCTION__);
}

void TimeControllingEventDispatcher::flush()
{
    throw NotImplemented(__FUNCTION__);
}

void TimeControllingEventDispatcher::progressTime(int timeToProgressInMs)
{
    Q_D(TimeControllingEventDispatcher);

    d->now += timeToProgressInMs;

    if (d->hasRegisteredTimers()) {
        QSharedPointer<TimerData> timer = d->nextTimerToActivate();
        while (timer->nextActivation < d->now) {
            d->activatedTimers.append(timer->timerID);
            timer->nextActivation += timer->interval;
            timer = d->nextTimerToActivate();
        }
    }
}

QSharedPointer<TimerData> TimeControllingEventDispatcherPrivate::nextTimerToActivate()
{
    int nextTimerId = timers.first()->timerID;
    int nextActivationTime = timers.first()->nextActivation;

    foreach (QSharedPointer<TimerData> const& data, timers) {
        if (data->nextActivation < nextActivationTime) {
            nextTimerId = data->timerID;
            nextActivationTime = data->nextActivation;
        }
    }

    return timers[nextTimerId];
}

bool TimeControllingEventDispatcherPrivate::hasRegisteredTimers() const
{
    return timers.size() > 0;
}
