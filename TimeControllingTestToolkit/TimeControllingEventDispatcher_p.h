#ifndef TIMECONTROLLINGEVENTDISPATCHER_P_H
#define TIMECONTROLLINGEVENTDISPATCHER_P_H



// todo: move to private header file
#include <QMap>
#include <QSharedPointer>

struct TimerData {
    int const timerID;
    QObject *const object;
    int const interval;
    Qt::TimerType const timerType;
    int nextActivation;

    TimerData(int timerId, QObject *object, int interval, Qt::TimerType timerType)
        : timerID(timerId)
        , object(object)
        , interval(interval)
        , timerType(timerType)
        , nextActivation(0)
    {
    }

    TimerData()
        : timerID(0)
        , object(0)
        , interval(0)
        , timerType(Qt::VeryCoarseTimer)
        , nextActivation(0)
    {
        // this constructor should not exist
        // still does as a workaround for QMap's need for a default constructor
        throw std::exception();
    }
};

class TimeControllingEventDispatcherPrivate {
public:
    QMap<int,QSharedPointer<TimerData> > timers;
    int now = 0;
    QList<int> activatedTimers;

    bool hasRegisteredTimers() const;
    QSharedPointer<TimerData> nextTimerToActivate();
};

#endif // TIMECONTROLLINGEVENTDISPATCHER_P_H
