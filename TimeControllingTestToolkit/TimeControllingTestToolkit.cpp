#include "TimeControllingTestToolkit.h"

void TimeControllingTestToolkit::progressTime(int timeToProgressInMs)
{
    TimeControllingEventDispatcher *eventDispatcher = qobject_cast<TimeControllingEventDispatcher*>(QCoreApplication::eventDispatcher());
    if (eventDispatcher == Q_NULLPTR) {
        throw std::logic_error("Not time controlled. Install EventDispatcher before instantiating QCoreApplication!");
    }

    eventDispatcher->progressTime(timeToProgressInMs);
    QCoreApplication::processEvents();

}
