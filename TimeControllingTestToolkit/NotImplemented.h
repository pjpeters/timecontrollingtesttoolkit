#ifndef NOTIMPLEMENTED_H
#define NOTIMPLEMENTED_H

#include <stdexcept>

class NotImplemented : public std::logic_error
{
public:
    NotImplemented(std::string function);
};

#endif // NOTIMPLEMENTED_H
