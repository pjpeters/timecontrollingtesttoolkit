#ifndef TIMECONTROLLINGEVENTDISPATCHER_H
#define TIMECONTROLLINGEVENTDISPATCHER_H

#include "timecontrollingtesttoolkit_global.h"

#include <QAbstractEventDispatcher>


class TimeControllingEventDispatcherPrivate;

class TIMECONTROLLINGTESTTOOLKITSHARED_EXPORT TimeControllingEventDispatcher : public QAbstractEventDispatcher {

    Q_OBJECT

public:
    TimeControllingEventDispatcher();
    ~TimeControllingEventDispatcher();

    bool processEvents(QEventLoop::ProcessEventsFlags flags) override;
    bool hasPendingEvents() override;

    void registerTimer(int timerId, int interval, Qt::TimerType timerType, QObject *object) override;
    bool unregisterTimer(int timerId) override;
    bool unregisterTimers(QObject *object) override;
    QList<TimerInfo> registeredTimers(QObject *object) const override;
    int remainingTime(int timerId) override;

    // these are not actually implemented and will throw an exception when called
    void registerSocketNotifier(QSocketNotifier *notifier) override;
    void unregisterSocketNotifier(QSocketNotifier *notifier) override;
    bool registerEventNotifier(QWinEventNotifier *notifier) override;
    void unregisterEventNotifier(QWinEventNotifier *notifier) override;
    void wakeUp() override;
    void interrupt() override;
    void flush() override;


    void progressTime(int timeToProgressInMs);

protected:
    QScopedPointer<TimeControllingEventDispatcherPrivate> d_ptr;

private:
    Q_DECLARE_PRIVATE(TimeControllingEventDispatcher)

};

#endif // TIMECONTROLLINGEVENTDISPATCHER_H
