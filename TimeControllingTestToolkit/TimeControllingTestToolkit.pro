#-------------------------------------------------
#
# Project created by QtCreator 2022-04-22T14:58:37
#
#-------------------------------------------------

QT       -= gui

TARGET = TimeControllingTestToolkit
TEMPLATE = lib

DEFINES += \
    TIMECONTROLLINGTESTTOOLKIT_LIBRARY \
    QT_DEPRECATED_WARNINGS \

SOURCES += \
    TimeControllingEventDispatcher.cpp \
    NotImplemented.cpp \
    TimeControllingTestToolkit.cpp \

HEADERS += \
    TimeControllingTestToolkit.h \
    timecontrollingtesttoolkit_global.h \
    TimeControllingEventDispatcher.h \
    TimeControllingEventDispatcher_p.h \
    NotImplemented.h \
