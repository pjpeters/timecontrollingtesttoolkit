#ifndef TIMECONTROLLINGTESTTOOLKIT_H
#define TIMECONTROLLINGTESTTOOLKIT_H

#include <QCoreApplication>

#include "timecontrollingtesttoolkit_global.h"
#include "TimeControllingEventDispatcher.h"

class TIMECONTROLLINGTESTTOOLKITSHARED_EXPORT TimeControllingTestToolkit {
public:
    static void progressTime(int timeToProgressInMs);
};

#define TIMECONTROLLED_QTEST_MAIN(TestObject) \
int main(int argc, char *argv[]) \
{ \
    QCoreApplication::setEventDispatcher(new TimeControllingEventDispatcher); \
 \
    QCoreApplication app(argc, argv); \
    app.setAttribute(Qt::AA_Use96Dpi, true); \
    TestObject tc; \
    QTEST_SET_MAIN_SOURCE_PATH \
    return QTest::qExec(&tc, argc, argv); \
}

#endif // TIMECONTROLLINGTESTTOOLKIT_H
