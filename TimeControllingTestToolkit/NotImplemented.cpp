#include "NotImplemented.h"

NotImplemented::NotImplemented(std::string function)
    : std::logic_error(std::string("not implemented: ") + function)
{
}
