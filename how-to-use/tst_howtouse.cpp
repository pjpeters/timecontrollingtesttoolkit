#include <QtTest>

#include <TimeControllingTestToolkit.h>

class test_timer_dependend_behavior : public QObject
{
    Q_OBJECT

public:
    test_timer_dependend_behavior();
    ~test_timer_dependend_behavior();

private slots:
    void test_case1();

private:
    int counter;

    void increaseCounter();
};

test_timer_dependend_behavior::test_timer_dependend_behavior()
    : counter(0)
{

}

test_timer_dependend_behavior::~test_timer_dependend_behavior()
{

}

void test_timer_dependend_behavior::test_case1()
{
    QTimer timer;
    timer.setInterval(100);
    connect(&timer, &QTimer::timeout, this, &test_timer_dependend_behavior::increaseCounter);

    timer.start();

    TimeControllingTestToolkit::progressTime(99);
    QCOMPARE(counter,0);

    TimeControllingTestToolkit::progressTime(2);
    QCOMPARE(counter,1);

}

void test_timer_dependend_behavior::increaseCounter()
{
    counter++;
}

TIMECONTROLLED_QTEST_MAIN(test_timer_dependend_behavior)

#include "tst_howtouse.moc"
