QT += testlib
QT -= gui

CONFIG += console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

INCLUDEPATH = ../TimeControllingTestToolkit


CONFIG( debug, debug|release ) {
    LIBS += -L../TimeControllingTestToolkit/debug \
} else {
    LIBS += -L../TimeControllingTestToolkit/release \
}

LIBS += -lTimeControllingTestToolkit \

SOURCES +=  tst_howtouse.cpp
